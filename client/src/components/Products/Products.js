import React, {useEffect, useState} from "react";
import styles from "./Products.module.css";
import axios from 'axios';

// Redux
import { connect } from "react-redux";

import Product from "./Product/Product";

const Products = ({ products }) => {
  const [itemList, setItemList] = useState([]);
  useEffect(() => {
    axios
      .get('/lists')
      .then(res => {
        console.log(res.data);
        setItemList(res.data);
      })
      .catch(err => {
        console.log(err)
      })

  }, [])
  return (
    <div className={styles.products}>
      {itemList.map((product) => (
        <Product key={product.id} product={product} />
      ))}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    products: state.shop.products,
  };
};

export default Products;
